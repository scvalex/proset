open Core

module Color = struct
  module T = struct
    type t =
      | Orange
      | Green
      | Purple
      | Red
      | Black
      | Blue
    [@@deriving compare, sexp]
  end

  include T
  include Comparable.Make(T)
  include Sexpable.To_stringable(T)

  let all =
    [ Orange; Green; Purple; Red; Black; Blue; ]
  ;;

  let to_int = function
    | Orange -> 0
    | Green  -> 1
    | Purple -> 2
    | Red    -> 3
    | Black  -> 4
    | Blue   -> 5
  ;;

  let to_rgb = function
    | Orange -> "#fc8b07"
    | Green  -> "#005029"
    | Purple -> "#d2016d"
    | Red    -> "#920303"
    | Black  -> "#060404"
    | Blue   -> "#313378"
  ;;
end

module Drawing = struct
  type xy = (float * float) [@@deriving sexp]

  type t =
    | Circle of Color.t
    | Scale of (xy * t)
    | Translate of (xy * t)
    | Concat of t list
  [@@deriving sexp]

  let to_svg ~width ~height t =
    let wrap before middle after =
      List.concat [[before]; middle; [after]]
    in
    let rec loop = function
      | Circle col ->
        [sprintf "<circle cx=\"0.5\" cy=\"0.5\" r=\"0.5\" fill=\"%s\" />" (Color.to_rgb col)
        (* sprintf "<rect width=\"1.0\" height=\"1.0\" style=\"fill:%s;\" />" (Color.to_rgb col) *)
        ]
      | Scale ((x, y), t) ->
        wrap
          (sprintf "<g transform=\"scale(%.2f,%.2f)\">" x y)
          (loop t)
          "</g>"
      | Translate ((x, y), t) ->
        wrap
          (sprintf "<g transform=\"translate(%.2f,%2f)\">" x y)
          (loop t)
          "</g>"
      | Concat ts ->
        List.concat_map ~f:loop ts
    in
    wrap
      (sprintf "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"%d\" height=\"%d\">"
         width height)
      (loop t)
      "</svg>"
  ;;
end

let all_sets () =
  let sets = ref [] in
  for i = 0 to Int.pow 2 6 - 1 do
    let set =
      Color.Set.of_list
        (List.filter Color.all ~f:(fun col ->
             Int.(0 <> i land (Int.pow 2 Color.(to_int col)))))
    in
    sets := set :: !sets
  done;
  !sets
;;

let print_set set =
  printf "%s\n"
    (Sexp.to_string_mach
       (String.Set.sexp_of_t
          (String.Set.map ~f:Color.to_string set)))
;;

let cmd_list () =
  List.iter (all_sets ()) ~f:print_set
;;

let cmd_svgs ~count ~output_dir ~include_empty ~width ~height ~pdf_gen ~easy =
  Random.self_init ();
  let sets =
    let all =
      if include_empty
      then all_sets ()
      else List.filter (all_sets ()) ~f:(fun set -> not (Set.is_empty set))
    in
    let sorted =
      if easy
      then
        List.sort all ~compare:(fun a b ->
            Int.compare (Set.length a) (Set.length b))
      else
        List.permute all
    in
    match count with
    | None       -> all
    | Some count -> List.take sorted count
  in
  let svg set =
    let open Color in
    let open Drawing in
    let drawing =
      let circ_s = 1.0 /. 3.0 in
      let circ col =
        Scale ((Float.(of_int height /. of_int width) *. circ_s, circ_s), Circle col)
      in
      let dh = (0.5 -. circ_s) /. 2.0 in
      let dw =
        (Float.(of_int width /. of_int height) *. 1.0 /. 3.0 -. circ_s) /. 2.0
        *. Float.(of_int height /. of_int width)
      in
      let circles =
        [ (Orange, Translate ((dw, dh), circ Orange))
        ; (Green, Translate ((1.0 /. 3.0 +. dw, dh), circ Green))
        ; (Purple, Translate ((2.0 /. 3.0 +. dw, dh), circ Purple))
        ; (Red, Translate ((dw, 0.5 +. dh), circ Red))
        ; (Black, Translate ((1.0 /. 3.0 +. dw, 0.5 +. dh), circ Black))
        ; (Blue, Translate ((2.0 /. 3.0 +. dw, 0.5 +. dh), circ Blue))
        ]
      in
      Concat
        (List.filter_map circles ~f:(fun (col, drawing) ->
             Option.some_if (Set.mem set col) drawing))
    in
    Drawing.to_svg ~width ~height
      (Scale ((Float.of_int width, Float.of_int height),
              (Translate ((0.1, 0.1), (Scale ((0.8, 0.8), drawing))))))
  in
  List.iteri sets ~f:(fun idx set ->
      let file = output_dir ^/ sprintf "front-%d" idx in
      Out_channel.write_lines (file ^ ".svg") (svg set);
      if pdf_gen then
        Sys.command_exn
          (sprintf "convert %s %s" (file ^ ".svg") (file ^ ".pdf")))
;;

let main () =
  let open Command.Let_syntax in
  Command.run
    (Command.group ~summary:"Proset -- A game of XORs"
       [ ("list-sets",
          Command.basic
            ~summary:"List all the sets"
            (let%map_open () = return ()
             in
             fun () -> 
               cmd_list ()))
       ; ("svgs",
          Command.basic
            ~summary:"Generate SVGs"
            (let%map_open count =
               flag "count" (optional int)
                 ~doc:"INT how many to output"
             and output_dir =
               flag "output-dir" (required Filename.arg_type)
                 ~doc:"DIR where to output the generated images"
             and include_empty = 
               flag "include-empty" no_arg
                 ~doc:" do NOT filter out the empty set"
             and width =
               flag "width" (required int)
                 ~doc:"INT card width in pixels"
             and height =
               flag "height" (required int)
                 ~doc:"INT card height in pixels"
             and pdf_gen = 
               flag "pdf-gen" no_arg
                 ~doc:" also generate PDFs"
             and easy =
               flag "easy" no_arg
                 ~doc:" output the unique cards with the fewest dots"
             in
             fun () ->
               cmd_svgs ~count ~output_dir ~include_empty ~width ~height ~pdf_gen ~easy))
       ])
;;

let () = main ();;
