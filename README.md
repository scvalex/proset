Proset
======

> A game to alienate your non-programmer friends.

Rules
-----

- Draw 7 cards and place them face-up.

- Match sets by selecting a group of cards that has an even number of
  each color dot.

- The game ends when there are less than 7 cards in the deck.

- The player with the largest set wins.

`proset.ml`
-----------

Install OCaml, OPAM, and Core.

To show all possible sets:

    coretop proset.ml list-sets

To generate cards:

    coretop proset.ml svgs -output-dir ~/tmp/proset \
      -count 49 -width 880 -height 590 -pdf
